﻿# Only uncomment this during development in ISE (which does not support this automatic variable).
#$PSScriptRoot = "C:\Users\nreiling\Repositories\SSMS Profile"

$TemplatesLocation   = "$env:APPDATA\Microsoft\SQL Server Management Studio\18.0\Templates\Sql\Tessitura Templates"
$TemplatesFolder = "$PSScriptRoot\Templates"
$SnippetsLocation   = "$env:USERPROFILE\Documents\SQL Server Management Studio\Code Snippets\SQL\My Code Snippets"
$SnippetsFolder = "$PSScriptRoot\Snippets"

$SubFolders = (Get-ChildItem -Path $SnippetsFolder -Directory).Name

# Use /K to observe command result; use /C to close window and exit.
$cmdargs = "/C rmdir /S /Q `"$TemplatesLocation`" & mklink /D `"$TemplatesLocation`" `"$TemplatesFolder`""

foreach ($FolderName in $SubFolders) {
    $cmdargs += " & rmdir /S /Q `"$SnippetsLocation\$FolderName`" & mklink /D `"$SnippetsLocation\$FolderName`" `"$SnippetsFolder\$FolderName`""
}

Write-Verbose "We're about to run (as administrator)> cmd.exe $cmdargs"

# We're doing this in just a single cmd call to avoid multiple Administrative Privileges prompts popping up from -Verb RunAs.
Start-Process cmd.exe -ArgumentList $cmdargs -Verb RunAs
