PRINT 'BEGIN INSTALL/UPDATE <type_name,sysname,TVP>'
GO

IF TYPE_ID('dbo.<type_name,sysname,TVP>') IS NOT NULL
BEGIN;
  -- Alter any dependent procedures here to allow drop.
  DROP TYPE [dbo].[<type_name,sysname,TVP>];
END;
GO

CREATE TYPE [dbo].[<type_name,sysname,TVP>] AS TABLE (
  [seq_no] int NOT NULL PRIMARY KEY,
  [value] varchar(4000) NULL
);
GO

GRANT EXECUTE ON TYPE::[dbo].[<type_name,sysname,TVP>] TO [ImpUsers], [tessitura_app];
GO
