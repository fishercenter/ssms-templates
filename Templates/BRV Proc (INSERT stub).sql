PRINT 'BEGIN INSTALL/UPDATE brv.<Procedure_Name, sysname, procedure_name>.sql';
GO

SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
GO

WITH r AS (SELECT d = '<Proc_Description, varchar(80), Find/Fix A Thing>', p = '<Procedure_Name, sysname, procedure_name>')
MERGE dbo.LTR_BRV_RULE t USING r ON t.brv_rule_procedure = r.p
  WHEN MATCHED AND t.description <> r.d THEN UPDATE SET t.description = r.d
  WHEN NOT MATCHED THEN INSERT (description, brv_rule_procedure) VALUES (r.d, r.p);
GO
/***********************************************************************************************************************
Author:       Nick Reilingh, Fisher Center at Bard College
Description:  <Prose_Description,,Description Here>

***********************************************************************************************************************/
CREATE OR ALTER PROCEDURE [brv].[<Procedure_Name, sysname, procedure_name>]
  @preview bit
AS
SET NOCOUNT, XACT_ABORT ON;

BEGIN TRANSACTION;

WITH inserting AS (

)
INSERT dbo.LTW_BRV_ISSUE
      (issue_desc, table_name, row_id, customer_no, issue_user, issue_dt)
SELECT issue_desc, table_name, row_id, customer_no, issue_user, issue_dt
  FROM inserting;

COMMIT TRANSACTION;

GO

/* END INSTALL/UPDATE <Procedure_Name, sysname, procedure_name> */
