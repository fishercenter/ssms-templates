PRINT 'BEGIN INSTALL <Table_Name, sysname, LTR_MY_REFTABLE>';
GO

IF OBJECT_ID(N'[dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>]', N'U') IS NOT NULL
BEGIN;
  SELECT * INTO [#<Table_Name, sysname, LTR_MY_REFTABLE>_TEMP] FROM [dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>];
  DROP TABLE [dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>];
END;
GO

/****** Object:  Table [dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>]   ******/
SET ANSI_NULLS, QUOTED_IDENTIFIER, ANSI_PADDING ON;
GO

CREATE TABLE [dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>] (
    [id] int NOT NULL,
    [description] varchar(30) NOT NULL,
    [inactive] char(1) NOT NULL CONSTRAINT [DF_<Table_Name, sysname, LTR_MY_REFTABLE>_inactive] DEFAULT ('N'),
    [created_by] varchar(8) NULL CONSTRAINT [DF_<Table_Name, sysname, LTR_MY_REFTABLE>_created_by] DEFAULT (dbo.FS_USER()),
    [create_dt] datetime NULL CONSTRAINT [DF_<Table_Name, sysname, LTR_MY_REFTABLE>_create_dt] DEFAULT (GETDATE()),
    [create_loc] varchar(16) NULL CONSTRAINT [DF_<Table_Name, sysname, LTR_MY_REFTABLE>_create_loc] DEFAULT (dbo.FS_LOCATION()),
    [last_updated_by] varchar(8) NULL CONSTRAINT [DF_<Table_Name, sysname, LTR_MY_REFTABLE>_last_updated_by] DEFAULT (dbo.FS_USER()),
    [last_update_dt] datetime NOT NULL CONSTRAINT [DF_<Table_Name, sysname, LTR_MY_REFTABLE>_last_update_dt] DEFAULT (GETDATE()),
  CONSTRAINT [PK_<Table_Name, sysname, LTR_MY_REFTABLE>] PRIMARY KEY CLUSTERED (
    [id] ASC
  ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
);
GO

CREATE TRIGGER [dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>_TU]
  ON [dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>]
  FOR UPDATE
AS
BEGIN;
SET NOCOUNT ON;
IF dbo.FS_GET_PARAM_FROM_APPNAME('LockTrigger') = 'Y'
  RETURN;
UPDATE  a
  SET   last_updated_by = [dbo].FS_USER(), last_update_dt = GETDATE()
  FROM  [dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>] a JOIN inserted b ON a.id = b.id;
END;
GO

EXEC dbo.UP_POPULATE_REFERENCE_METADATA '<Table_Name, sysname, LTR_MY_REFTABLE>'; /* -- Use below if desired.
UPDATE dbo.TR_REFERENCE_TABLE
  SET help_text = 'Rows can be added to this table using the Add Conditional Schedule utility. ' +
                  'This table can be used to change settings, inactivate, and delete conditional schedules. ' +
                  'The native schedule SHOULD be left deactivated to avoid duplicate reports.'
  WHERE table_name = '<Table_Name, sysname, LTR_MY_REFTABLE>';
WITH metadata AS (
  SELECT rt.id, v.*
    FROM (VALUES
      ('schedule_id', 'display', 'N', 'gooesoft_report_schedule', 'id', 'name', 'deactivate_flag = ''d'''),
      ('report_id', 'display', 'N', 'gooesoft_report', 'id', 'name', NULL),
      ('condition_id', 'display', 'N', 'LTR_CRS_REPORT_CONDITION', 'id', 'description', NULL),
      ('run_at_schedule_time', 'editable', 'Y', NULL, NULL, NULL, NULL)
    ) v (column_name, column_type, checkbox, dddw_table, dddw_value, dddw_description, dddw_where)
      JOIN dbo.TR_REFERENCE_TABLE rt ON rt.table_name = '<Table_Name, sysname, LTR_MY_REFTABLE>'
)
MERGE dbo.TR_REFERENCE_COLUMN c
  USING metadata m
    ON c.reference_table_id = m.id AND c.column_name = m.column_name
  WHEN MATCHED THEN UPDATE
    SET c.column_type = m.column_type,
        c.checkbox = m.checkbox,
        c.dddw_table = m.dddw_table,
        c.dddw_value = m.dddw_value,
        c.dddw_description = m.dddw_description,
        c.dddw_where = m.dddw_where; */
GO

GRANT SELECT, INSERT, UPDATE, DELETE ON [dbo].[<Table_Name, sysname, LTR_MY_REFTABLE>] TO [ImpUsers], [tessitura_app];
GO

IF OBJECT_ID('tempdb..#<Table_Name, sysname, LTR_MY_REFTABLE>_TEMP') IS NOT NULL
BEGIN;
  BEGIN TRY
    EXEC('INSERT INTO dbo.<Table_Name, sysname, LTR_MY_REFTABLE> SELECT * FROM #<Table_Name, sysname, LTR_MY_REFTABLE>_10;');
  END TRY
  BEGIN CATCH
    IF @@ERROR = 208 -- If migration table doesn't exist, then we didn't advance versions.
      EXEC('INSERT INTO dbo.<Table_Name, sysname, LTR_MY_REFTABLE> SELECT * FROM #<Table_Name, sysname, LTR_MY_REFTABLE>_TEMP;');
    ELSE
      THROW;
  END CATCH;
  DROP TABLE [#<Table_Name, sysname, LTR_MY_REFTABLE>_TEMP];
END;
GO

/* END INSTALL <Table_Name, sysname, LTR_MY_REFTABLE> */
