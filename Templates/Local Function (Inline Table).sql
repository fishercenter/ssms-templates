PRINT 'BEGIN INSTALL/UPDATE LFT_<Inline_Table-Valued_Function_Name, sysname, MY_FUNCTION>';
GO

SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
GO

/***********************************************************************************************************************
Author:       Nick Reilingh, Fisher Center at Bard College
Description:  <Function_Description,,Description Here>

***********************************************************************************************************************/
CREATE OR ALTER FUNCTION [dbo].[LFT_<Inline_Table-Valued_Function_Name, sysname, MY_FUNCTION>](
  @parameter int
)
RETURNS TABLE AS RETURN
SELECT 1 AS col;
GO

GRANT SELECT ON [dbo].[LFT_<Inline_Table-Valued_Function_Name, sysname, MY_FUNCTION>] TO [ImpUsers], [tessitura_app];
GO
/* END INSTALL/UPDATE LFT_<Inline_Table-Valued_Function_Name, sysname, MY_FUNCTION> */
