-- BEGIN <Object_Name,sysname,LP_OBJECT_NAME>.t.sql

EXEC tSQLt.NewTestClass 't<Object_Name,sysname,LP_OBJECT_NAME>';
GO

-- ALL TEST OBJECTS must begin with the word 'test '
CREATE PROCEDURE [t<Object_Name,sysname,LP_OBJECT_NAME>].[test <First_test_name,,some behavior>]
AS
BEGIN
  -- Assemble

  -- Act

  -- Assert
  EXEC tSQLt.Fail 'TODO: Implement this test.';
END;
GO

-- Uncomment during development if desired.
--EXEC tSQLt.Run 't<Object_Name,sysname,LP_OBJECT_NAME>';
