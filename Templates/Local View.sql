PRINT 'BEGIN INSTALL/UPDATE <View_Name, sysname, LV_MY_VIEW>';
GO

SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
GO

/***********************************************************************************************************************
Author:       Nick Reilingh, Fisher Center at Bard College
Description:  <View_Description,,Description Here>

***********************************************************************************************************************/
CREATE OR ALTER VIEW [dbo].[<View_Name, sysname, LV_MY_VIEW>]
AS

SELECT 1 col;

GO

GRANT SELECT ON [dbo].[<View_Name, sysname, LV_MY_VIEW>] TO [ImpUsers], [tessitura_app];
GO
/* END INSTALL/UPDATE <View_Name, sysname, LV_MY_VIEW> */
