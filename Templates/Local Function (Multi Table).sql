PRINT 'BEGIN INSTALL/UPDATE LFT_<Multistatement_Table-Valued_Function_Name, sysname, MY_FUNCTION>';
GO

SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
GO

/***********************************************************************************************************************
Author:       Nick Reilingh, Fisher Center at Bard College
Description:  <Function_Description,,Description Here>

***********************************************************************************************************************/
CREATE OR ALTER FUNCTION [dbo].[LFT_<Multistatement_Table-Valued_Function_Name, sysname, MY_FUNCTION>](
  @parameter int
)
RETURNS @return TABLE (
  id int
)
AS
BEGIN

RETURN;

END;
GO

GRANT SELECT ON [dbo].[LFT_<Multistatement_Table-Valued_Function_Name, sysname, MY_FUNCTION>] TO [ImpUsers], [tessitura_app];
GO
/* END INSTALL/UPDATE LFT_<Multistatement_Table-Valued_Function_Name, sysname, MY_FUNCTION> */
