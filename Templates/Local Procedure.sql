PRINT 'BEGIN INSTALL/UPDATE <Procedure_Name, sysname, LP_MY_PROCEDURE>';
GO

SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
GO

/***********************************************************************************************************************
Author:       Nick Reilingh, Fisher Center at Bard College
Description:  <Procedure_Description,,Description Here>

Procedure:
  - Whirrs
  - Buzzes
  - Toots
  - Floots
***********************************************************************************************************************/
CREATE OR ALTER PROCEDURE [dbo].[<Procedure_Name, sysname, LP_MY_PROCEDURE>]
  @parameter int = NULL
AS
SET NOCOUNT, XACT_ABORT ON;

SELECT 1;

GO

GRANT EXECUTE ON [dbo].[<Procedure_Name, sysname, LP_MY_PROCEDURE>] TO [ImpUsers], [tessitura_app];
GO
/* END INSTALL/UPDATE <Procedure_Name, sysname, LP_MY_PROCEDURE> */
