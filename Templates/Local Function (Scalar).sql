PRINT 'BEGIN INSTALL/UPDATE LFS_<Scalar_Function_Name, sysname, MY_FUNCTION>';
GO

SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
GO

/****************************************************************************************************************************
Author:       Nick Reilingh, Fisher Center at Bard College
Description:  <Function_Description,,Description Here>

****************************************************************************************************************************/
CREATE OR ALTER FUNCTION [dbo].[LFS_<Scalar_Function_Name, sysname, MY_FUNCTION>](
  @parameter int
)
RETURNS <Function_Data_Type, sql_variant, int>
AS
BEGIN

RETURN 0;

END;
GO

GRANT EXECUTE ON [dbo].[LFS_<Scalar_Function_Name, sysname, MY_FUNCTION>] TO [ImpUsers], [tessitura_app];
GO
/* END INSTALL/UPDATE LFS_<Scalar_Function_Name, sysname, MY_FUNCTION> */
