/* BEGIN INSTALL/UPDATE brv.<Procedure_Name, sysname, procedure_name>.sql */
USE [impresario];
GO

SET ANSI_NULLS, QUOTED_IDENTIFIER ON;
GO

WITH r AS (SELECT d = '<Proc_Description, varchar(80), Find/Fix A Thing>', p = '<Procedure_Name, sysname, procedure_name>')
MERGE dbo.LTR_BRV_RULE t USING r ON t.brv_rule_procedure = r.p
  WHEN MATCHED AND t.description <> r.d THEN UPDATE SET t.description = r.d
  WHEN NOT MATCHED THEN INSERT (description, brv_rule_procedure) VALUES (r.d, r.p);
GO
/***********************************************************************************************************************
Author:       Nick Reilingh, Fisher Center at Bard College
Description:  <Prose_Description,,Description Here>

Report rules by inserting to dbo.LTW_BRV_ISSUE as follows:
  issue_desc varchar(max) -- This column is always required; others are all optional. First 140 characters may be logged
  table_name sysname      -- May be used to indicate the problem table.
  row_id int              -- May be used to indicate the problem row ID.
  customer_no int         -- Not logged; this column allows us to create a clickable link in an SSRS report.
  issue_user varchar(8)   -- May be used to indicate the last_updated_by value for the problem row.
  issue_dt datetime       -- May be used to indicate the last_update_dt value for the problem row.
  resolved bit            -- Indicates whether the issue has been solved programmatically here. Default false.
  write_log bit           -- If false, don't write to permanent log table. Default true.

You may not alter data if the @preview bit is true.
Likewise, data will only be written to the permanent log if @preview is false.
An issue is uniquely identified by LEFT(issue_desc, 140), table_name, row_id, issue_user, and issue_dt.
Existing entries in the permanent log table will be updated if they match on these fields.
***********************************************************************************************************************/
CREATE OR ALTER PROCEDURE [brv].[<Procedure_Name, sysname, procedure_name>]
  @preview bit
AS
SET NOCOUNT, XACT_ABORT ON;

BEGIN TRANSACTION;

-- Find and insert pattern:
WITH inserting AS (
  SELECT
      issue_desc  = 'Description is the only mandatory field without a default.',
      table_name  = NULL,
      row_id      = NULL,
      customer_no = NULL,
      issue_user  = NULL,
      issue_dt    = NULL
    FROM dbo.sometable
    WHERE criteria = 'bad'
)
INSERT dbo.LTW_BRV_ISSUE
      (issue_desc, table_name, row_id, customer_no, issue_user, issue_dt)
SELECT issue_desc, table_name, row_id, customer_no, issue_user, issue_dt
  FROM inserting;

-- Update pattern
DECLARE @issues TABLE (
  issue_desc varchar(max) NOT NULL,
  table_name sysname NULL,
  row_id int NULL,
  customer_no int NULL,
  issue_user varchar(8) NULL,
  issue_dt datetime NULL,
  resolved bit NULL,
  write_log bit NULL );

SAVE TRANSACTION pre_update;

UPDATE t -- table
    SET col = 'correction'-- expression
    OUTPUT
        'Description is the only mandatory field without a default.' AS issue_desc,
        NULL AS table_name,
        NULL AS row_id,
        NULL AS customer_no,
        NULL AS issue_user,
        NULL AS issue_dt,
        1 AS resolved
      INTO @issues (issue_desc, table_name, row_id, customer_no, issue_user, issue_dt, resolved)
    FROM dbo.tablename t
  WHERE criteria = 'bad';

IF @preview = 1
  ROLLBACK TRANSACTION pre_update;

INSERT dbo.LTW_BRV_ISSUE
      (issue_desc, table_name, row_id, issue_user, issue_dt, resolved)
SELECT issue_desc, table_name, row_id, issue_user, issue_dt, resolved
  FROM @issues;

COMMIT TRANSACTION;

GO

/* END INSTALL/UPDATE <Procedure_Name, sysname, procedure_name> */
